import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.*;

public class App {
    public static void main(String[] args) throws Exception {
        //1 Tạo mới 1 ArrayList Integer, thêm 10 số(kiểu int) và ghi ra terminal ArrayList vừa tạo. Sắp xếp các phần tử theo thứ tự giá trị tăng dần và ghi lại ra terminal ArrayList vừa được sắp xếp
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        list1.add(19);
        list1.add(9);
        list1.add(1);
        list1.add(300);
        list1.add(7);
        list1.add(6);
        list1.add(8);
        list1.add(2);
        list1.add(4);
        list1.add(0);
        System.out.println("1. ArrayList before sort: " + list1);
        Collections.sort(list1);
        System.out.println("1. ArrayList after sort: " + list1);

        //2 Tạo mới 1 ArrayList Integer, thêm 10 số(kiểu int) và ghi ra terminal ArrayList vừa tạo. Tạo một ArrayList mới chứa các số được lấy từ ArrayList trên có giá trị từ 10 đến 100 và ghi ra terminal ArrayList mới này.
        ArrayList<Integer> list2 = new ArrayList<Integer>();
        list2.add(5);
        list2.add(9);
        list2.add(10);
        list2.add(3);
        list2.add(7);
        list2.add(100);
        list2.add(81);
        list2.add(2);
        list2.add(44);
        list2.add(0);
        System.out.println("2. ArrayList: " + list2);
        ArrayList<Integer> filteredlist2 = new ArrayList<Integer>();
        for (int number : list2) {
            if (number >= 10 && number <= 100) {
                filteredlist2.add(number);
            }
        }
            System.out.println("2. ArrayList from 10 to 100: " + filteredlist2 );

        //3 Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Kiểm tra ArrayList này nếu chứa màu vàng thì in ra terminal chữ “OK” còn ngược lại in ra chữ “KO”
        ArrayList<String> colors3 = new ArrayList<String>();
        colors3.add("Red");
        colors3.add("Blue");
        colors3.add("Green");
        colors3.add("Yellow");
        colors3.add("Purple");

        System.out.println("3. ArrayList colors: " + colors3);

        if (colors3.contains("Yellow")) {
            System.out.println("OK");
        } else {
            System.out.println("KO");
        }

        //4 Tạo mới 1 ArrayList Integer, thêm 10 số(kiểu int) và ghi ra terminal ArrayList vừa tạo. Ghi ra terminal tổng của các số trong ArrayList
        ArrayList<Integer> numbers4 = new ArrayList<Integer>();
        numbers4.add(5);
        numbers4.add(9);
        numbers4.add(1);
        numbers4.add(3);
        numbers4.add(7);
        numbers4.add(6);
        numbers4.add(8);
        numbers4.add(2);
        numbers4.add(4);
        numbers4.add(0);
        System.out.println("4. ArrayList: " + numbers4);
        int sum4 = 0;
        for (int number : numbers4) {
            sum4 += number;
        }
        System.out.println("4. Sum of number list: " + sum4);

        //5 Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Xóa bỏ toàn bộ giá trị của ArrayList và ghi lại ra terminal giá trị mới của ArrayList này.
        ArrayList<String> colors5 = new ArrayList<String>();
        colors5.add("Red");
        colors5.add("Blue");
        colors5.add("Green");
        colors5.add("Yellow");
        colors5.add("Purple");
        System.out.println("5. ArrayList: " + colors5);
        colors5.clear();
        System.out.println("5. ArrayList after clearing: " + colors5);

        //6 Tạo mới 1 ArrayList String, thêm 10 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Hoán đổi ngẫu nhiên vị trí các phần tử của ArrayList và ghi lại ra terminal giá trị mới của ArrayList này.
        ArrayList<String> colors6 = new ArrayList<String>();
        colors6.add("Red");
        colors6.add("Blue");
        colors6.add("Green");
        colors6.add("Yellow");
        colors6.add("Purple");
        colors6.add("Orange");
        colors6.add("Pink");
        colors6.add("Black");
        colors6.add("White");
        colors6.add("Gray");
        System.out.println("6.ArrayList: " + colors6);
        Collections.shuffle(colors6);
        System.out.println("6. ArrayList atter shuffle: " + colors6);

        //7 Tạo mới 1 ArrayList String, thêm 10 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Đảo ngược vị trí các phần tử của ArrayList và ghi lại ra terminal giá trị mới của ArrayList này.
        ArrayList<String> colors7 = new ArrayList<String>();
        colors7.add("Red");
        colors7.add("Orange");
        colors7.add("Yellow");
        colors7.add("Green");
        colors7.add("Blue");
        colors7.add("Indigo");
        colors7.add("Violet");
        colors7.add("Pink");
        colors7.add("Brown");
        colors7.add("Black");
        System.out.println("7.Arraylist before reverse: " + colors7);
        Collections.reverse(colors7);
        System.out.println("7.Arraylist after reverse: " + colors7);

        //8 Tạo mới 1 ArrayList String, thêm 10 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Ghi ra terminal một List mới được cắt từ phần tử thứ 3  => 7 của ArrayList trên
        ArrayList<String> colors8 = new ArrayList<String>();
        colors8.add("Red");
        colors8.add("Orange");
        colors8.add("Yellow");
        colors8.add("Green");
        colors8.add("Blue");
        colors8.add("Indigo");
        colors8.add("Violet");
        colors8.add("Pink");
        colors8.add("Brown");
        colors8.add("Black");
        System.out.println("8. Arraylist: "+colors8);
        List<String> subList = colors8.subList(2, 7);
        System.out.println("8. Arraylist after cut: "+ subList);

        //9 Tạo mới 1 ArrayList String, thêm 10 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Hoán đổi vị trí của phần tử thứ 3 với phần tử thứ 7 trong ArrayList và ghi lại ra terminal giá trị mới của ArrayList này.
        ArrayList<String> colors9 = new ArrayList<String>();
        colors9.add("Red");
        colors9.add("Orange");
        colors9.add("Yellow");
        colors9.add("Green");
        colors9.add("Blue");
        colors9.add("Indigo");
        colors9.add("Violet");
        colors9.add("Pink");
        colors9.add("Brown");
        colors9.add("Black");
        System.out.println("9. Arraylist: " + colors9);
        Collections.swap(colors9, 2, 6);
        System.out.println("8. Arraylist after switch index: "+ colors9);

        //10 Tạo mới 2 ArrayList Integer, thêm 3 số (kiểu int) vào mỗi ArrayList, ghi ra terminal 2 ArrayList này. Copy một ArrayList vào ArrayList còn lại và ghi lại ra terminal 2 ArrayList này.
        ArrayList<Integer> list101 = new ArrayList<Integer>();
        list101.add(1);
        list101.add(2);
        list101.add(3);

        ArrayList<Integer> list102 = new ArrayList<Integer>();
        list102.add(4);
        list102.add(5);
        list102.add(6);

        System.out.println("10. Arraylist1: " + list101);
        System.out.println("10. Arraylist2: " + list102);

        list102.addAll(list101);

        System.out.println("10. Arraylist1 after copy: " + list101);
        System.out.println("10. Arraylist2 after copy: " + list102);
    }
}

